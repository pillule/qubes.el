<!--- Local IspellDict: en -->

This [GNU Emacs](https://www.gnu.org/software/emacs/) library
provides [Qubes OS](https://www.qubes-os.org/) integration, in
particular disposable virtual machine (dispVM) functionality inside
[Gnus](https://www.gnu.org/software/emacs/manual/html_node/gnus/index.html)
and
[Dired](https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html).

The comments at the beginning of [qubes.el](qubes.el) should get you
started.  Also, customizable variables appear towards the top of that
file, and they are grouped under `qubes`.

A [blog post](https://blogs.fsfe.org/jens.lechtenboerger/2017/04/12/gnu-emacs-under-qubes-os/)
contains some context information.
